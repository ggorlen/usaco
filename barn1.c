/*
ID: gsgorle1
LANG: C
TASK: barn1
*/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int cmp(const void *a, const void *b) {
  return *(int*)a - *(int*)b;
}

int main() {
    int i, m, s, c, range;
    FILE *fin = fopen("barn1.in", "r");
    FILE *fout = fopen("barn1.out", "w");
    fscanf(fin, "%d %d %d", &m, &s, &c);

    if (m >= c) {
        fprintf(fout, "%d\n", c);
        fclose(fin);
        fclose(fout);
        return 0;
    }

    int occupied[c];
    int gaps[c-1];
    int lo = INT_MAX; 
    int hi = 0;

    for (i = 0; i < c; i++) {
        fscanf(fin, "%d", &occupied[i]);

        if (occupied[i] < lo) {
            lo = occupied[i];
        }
        else if (occupied[i] > hi) {
            hi = occupied[i];
        }
    }

    qsort(occupied, c, sizeof(int), cmp);

    for (i = 0; i < c - 1; i++) {
        gaps[i] = occupied[i+1] - occupied[i] - 1;
    }
    
    qsort(gaps, c - 1, sizeof(int), cmp);
    range = hi - lo + 1;

    for (i = c - 2; i > c - 1 - m; i--) {
        range -= gaps[i];
    }

    fprintf(fout, "%d\n", range);
    fclose(fin);
    fclose(fout);
    return 0;
}
