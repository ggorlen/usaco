/*
ID: gsgorle1
LANG: C
TASK: milk
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    unsigned int price;
    unsigned int amount;
} Farmer;

int cmp(const void *a, const void *b) {
  return ((Farmer *)a)->price - ((Farmer *)b)->price;
}

int main() {
    int i, n, m;
    int spent = 0;
    FILE *fin = fopen("milk.in", "r");
    FILE *fout = fopen("milk.out", "w");
    fscanf(fin, "%d %d", &n, &m);
    Farmer farmers[m];

    for (i = 0; i < m; i++) {
        Farmer f;
        fscanf(fin, "%d %d", &f.price, &f.amount);
        farmers[i] = f;
    }

    qsort(farmers, m, sizeof(Farmer), cmp);

    for (i = 0; n > 0; i++) {
        if (n < farmers[i].amount) {
            farmers[i].amount = n;
        }

        n -= farmers[i].amount;
        spent += farmers[i].price * farmers[i].amount;
    }
    
    fprintf(fout, "%d\n", spent);
    fclose(fin);
    fclose(fout);
    return 0;
}
