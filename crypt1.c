/*
ID: gsgorle1
LANG: C
TASK: crypt1
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int in_arr(int target, int len, int *a) {
    int i;

    for (i = 0; i < len; i++) {
        if (a[i] == target) { return 1; }
    }

    return 0;
}

int solved(int f_len, int d_len, int *fields, int *digits) {
    int i, p;
    int a[3];
    int b[3];
    int carry = 0;

    for (i = 2; i < f_len; i++) {
        p = fields[0] * fields[i] + carry;

        if (p > 9) {
            carry = p / 10;
        }
        else {
            carry = 0;
        }

        a[i-2] = p % 10;

        if (!in_arr(a[i-2], d_len, digits)) { 
            return 0; 
        }
    }

    if (carry) { return 0; }

    for (i = 2; i < f_len; i++) {
        p = fields[1] * fields[i] + carry;

        if (p > 9) {
            carry = p / 10;
        }
        else {
            carry = 0;
        }

        b[i-2] = p % 10;

        if (!in_arr(b[i-2], d_len, digits)) { 
            return 0; 
        }
    }

    if (carry) { return 0; }

    int result[4];
    result[0] = a[0];

    for (i = 0; i < 2; i++) {
        result[i+1] = a[i+1] + b[i] + carry;

        if (result[i+1] > 9) {
            result[i+1] -= 10;
            carry = 1;
        }
        else {
            carry = 0;
        }

        if (!in_arr(result[i+1], d_len, digits)) { 
            return 0; 
        }
    }

    result[3] = carry + b[2];

    for (i = 0; i < 4; i++) {
        if (!in_arr(result[i], d_len, digits)) {
            return 0;
        }
    }
    
    return result[3] <= 9;
}

int count_solns(int i, int f_len, int d_len, int *fields, int *digits) {
    int j;
    int solns = 0;

    if (i < f_len) {
        for (j = 0; j < d_len; j++) {
            fields[i] = digits[j];
            
            if (i == f_len - 1 && solved(f_len, d_len, fields, digits)) {
                solns++; 
            }

            solns += count_solns(i + 1, f_len, d_len, fields, digits);
        }
    }

    return solns;
}

int main() {
    int i, n;
    int solns = 0;
    FILE *fin = fopen("crypt1.in", "r");
    FILE *fout = fopen("crypt1.out", "w");
    fscanf(fin, "%d\n", &n);
    int digits[n];
    int fields[5];
    char line[n*2];
    fgets(line, n * 2, fin);
    char *token = strtok(line, " ");
   
    for (i = 0; token; i++) {
        digits[i] = atoi(token);
        token = strtok(NULL, " ");
    }

    for (i = 0; i < 5; i++) {
        fields[i] = digits[0];
    }

    fprintf(fout, "%d\n", count_solns(0, 5, n, fields, digits));
    fclose(fin);
    fclose(fout);
    return 0;
}
