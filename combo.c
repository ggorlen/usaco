/*
ID: gsgorle1
LANG: C
TASK: combo
*/

#include <stdio.h>

int main() {
    int h, i, j, k, l, n, x, y, z, valid;
    int a[6];
    int seen[1000][3];
    int combos = 0;
    FILE *fin = fopen("combo.in", "r");
    FILE *fout = fopen("combo.out", "w");
    fscanf(
        fin, "%d\n%d %d %d\n%d %d %d", &n, 
        &a[0], &a[1], &a[2], &a[3], &a[4], &a[5]
    );

    for (h = 0; h < 6; h += 3) {
        for (i = a[h] - 2; i < a[h] + 3; i++) {
            for (j = a[h+1] - 2; j < a[h+1] + 3; j++) {
                for (k = a[h+2] - 2; k < a[h+2] + 3; k++) {
                    valid = 1, x = i, y = j, z = k;
                    
                    if (x <= 0) { x += n; }
                    if (y <= 0) { y += n; }
                    if (z <= 0) { z += n; }
                    x %= n;
                    y %= n;
                    z %= n;
        
                    for (l = 0; l < combos && valid; l++) {
                        if (seen[l][0] == x && 
                            seen[l][1] == y && 
                            seen[l][2] == z) {
                            valid = 0;
                        }
                    }
        
                    if (valid) {
                        seen[combos][0] = x;
                        seen[combos][1] = y;
                        seen[combos][2] = z;
                        combos++;
                    }
                }
            }
        }
    }

    fprintf(fout, "%d\n", combos);
    fclose(fin);
    fclose(fout);
    return 0;
}
