/*
ID: gsgorle1
LANG: C
TASK: gift1
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    FILE *fin  = fopen("gift1.in", "r");
    FILE *fout = fopen("gift1.out", "w");
    char name[16];
    char giver[16];
    char recipient[16];
    int i, j, k, np, money, numGifts;
    fscanf(fin, "%d", &np);

    char **people = malloc(sizeof(*people) * np);
    int *cash = malloc(sizeof(*cash) * np);

    for (i = 0; i < np; i++) {
        cash[i] = 0;
        fscanf(fin, "%s", &name);
        people[i] = malloc(sizeof(people[i]) * (strlen(name) + 1));
        strcpy(people[i], name);
    }

    for (i = 0; i < np; i++) {
        fscanf(fin, "%s", &giver);
        fscanf(fin, "%d %d", &money, &numGifts);
        
        if (money == 0) {
            continue;
        }

        for (j = 0; j < np; j++) {
            if (strcmp(people[j], giver) == 0) {
                cash[j] += money % numGifts - money;
                break;
            }
        }

        for (j = 0; j < numGifts; j++) {
            fscanf(fin, "%s", &recipient);

            for (k = 0; k < np; k++) {
                if (strcmp(people[k], recipient) == 0) {
                    cash[k] += money / numGifts;
                    break;
                }
            }
        }
    }

    for (i = 0; i < np; i++) {
        fprintf(fout, "%s %d\n", people[i], cash[i]);
    }

    fclose(fin);
    fclose(fout);
    exit(0);
    return 0;
}
