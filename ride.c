/*
ID: gsgorle1
LANG: C
TASK: ride
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *fin = fopen("ride.in", "r");
    FILE *fout = fopen("ride.out", "w");
    char a[7];
    char b[7];

    fscanf(fin, "%s\n%s\n", a, b);
    fprintf(fout, "%s\n", prod(&a) % 47 == prod(&b) % 47 ? "GO" : "STAY");
 
    fclose(fin);
    fclose(fout);
    return 0;
}

int prod(char *s) {
    int product = 1;

    while (*s != '\0') {
        product *= (*s - 64);
        s++;
    }

    return product;
}
