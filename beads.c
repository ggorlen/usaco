/*
ID: gsgorle1
LANG: C
TASK: beads 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    int i, n;
    char beads[352];
    FILE *fin = fopen("beads.in", "r");
    FILE *fout = fopen("beads.out", "w");
    fscanf(fin, "%d", &n);
    fscanf(fin, "%s", &beads);
    
    int longest = 0;
    
    for (i = 0; i < n; i++) {
        int run = 0;
        int j = i;
        char target = beads[j];
        
        while (beads[j] == 'w' || beads[j] == target || 
               beads[j] != target && target == 'w') {
            if (target == 'w') {
                target = beads[j];
            }

            run++;
            j = (j + 1) % n;

            if (i == j) { break; }
        }

        if (--j < 0) { j = n - 1; }

        int k = i - 1;

        if (j - k != 0) {
            if (k < 0) { 
                k = n - 1; 
            }
            
            target = beads[k];
            
            while ((beads[k] == 'w' || beads[k] == target || 
                    beads[k] != target && target == 'w') && k != j) {
                if (target == 'w') {
                    target = beads[k];
                }

                run++;

                if (--k < 0) { 
                    k = n - 1; 
                }
            }
        }

        if (run > longest) { longest = run; }
    }

    fprintf(fout, "%d\n", longest);
    fclose(fin);
    fclose(fout);
    return 0;
}
