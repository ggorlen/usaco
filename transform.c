/*
ID: gsgorle1
LANG: C
TASK: transform
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void freeMatrix(char **m, int n) {
    int i;

    for (i = 0; i < n; i++) {
        free(m[i]);
    }

    free(m);
}

char **rotCW(char **m, int n) {
    int i, j, x, y;
    char **tmp = malloc(sizeof(tmp) * n);

    for (i = 0; i < n; i++) {
        tmp[i] = malloc(sizeof(tmp[i]) * n);
    }

    for (i = 0, y = 0; i < n; i++, y++) {
        for (j = 0, x = n - 1; j < n;) {
            tmp[y][x--] = m[j++][i];
        }
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            m[i][j] = tmp[i][j];
        }
    }

    freeMatrix(tmp, n);
    return m;
}

char **hRef(char **m, int n) {
    int i, j, tmp;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n / 2; j++) {
            tmp = m[i][j];
            m[i][j] = m[i][n-j-1];
            m[i][n-j-1] = tmp;
        }
    }

    return m;
}

int eq(char **a, char **b, int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (a[i][j] != b[i][j]) {
                return 0;
            }
        }
    }

    return 1;
}

void print(char **m, int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("[%c]", m[i][j]);
        }

        puts("");
    }

    puts("");
}

int evaluate(char **a, char **b, int n) {
    if (eq(rotCW(a, n), b, n)) {
        return 1;
    }
    else if (eq(rotCW(a, n), b, n)) {
        return 2;
    }
    else if (eq(rotCW(a, n), b, n)) {
        return 3;
    }
    else if (eq(hRef(rotCW(a, n), n), b, n)) {
        return 4;
    }
    else if (eq(rotCW(a, n), b, n) || 
             eq(rotCW(a, n), b, n) ||
             eq(rotCW(a, n), b, n)) {
        return 5;
    }
    else if (eq(hRef(rotCW(a, n), n), b, n)) {
        return 6;
    }
    
    return 7;
}

char **readMatrix(FILE *fin, int n) {
    int i, j;
    char **m = malloc(sizeof(m) * n);

    for (i = 0; i < n; i++) {
        m[i] = malloc(sizeof(m[i]) * n);

        for (j = 0; j < n; j++) {
            fscanf(fin, " %c ", &m[i][j]);
        }
    }

    return m;
}

int main() {
    int n;
    FILE *fin = fopen("transform.in", "r");
    FILE *fout = fopen("transform.out", "w");
    fscanf(fin, "%d", &n);
    char **a = readMatrix(fin, n);
    char **b = readMatrix(fin, n);

    fprintf(fout, "%d\n", evaluate(a, b, n));

    fclose(fin);
    fclose(fout);
    freeMatrix(a, n);
    freeMatrix(b, n);
    return 0;
}
