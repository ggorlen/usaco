/*
ID: gsgorle1
LANG: C
TASK: friday
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *fin  = fopen("friday.in", "r");
    FILE *fout = fopen("friday.out", "w");
    int n, i, j, k;
    int thirteenths[7] = {0};
                        //jan feb mar apr may jun jul aug sep oct nov dec
    int monthSizes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int week = 7;
    int months = 12;
    int year = 1900;
    int dayOfWeek = 1;

    fscanf(fin, "%d", &n);
    
    for (i = 0; i < n; i++) {
        monthSizes[1] = 28;

        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthSizes[1]++;
        }
        
        for (j = 0; j < months; j++) {
            dayOfWeek += 13;
            thirteenths[dayOfWeek%week]++;
            dayOfWeek += monthSizes[j] - 13;
        }
        
        year++;
    }

    for (i = 0; i < week - 1; i++) {
        fprintf(fout, "%d ", thirteenths[i]);
    }

    fprintf(fout, "%d\n", thirteenths[week-1]); // end in a newline

    fclose(fin);
    fclose(fout);
    return 0;
}
