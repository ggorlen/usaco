/*
ID: gsgorle1
LANG: C
TASK: dualpal
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int palindromic(char *s) {
    int i;
    int len = strlen(s);
    char buf[len];

    for (i = 0; i < len; i++) {
        buf[i] = s[i];
    }
    
    int half_len = len / 2;

    for (i = 0; i < half_len; i++) {
        if (buf[i] != buf[len-i-1]) {
            return 0;
        }
    }

    return 1;
}

char *to_base(int n, int b) {
    int i;
    char *result = malloc(sizeof(*result) * 80);

    for (i = 0; n > 0; i++) {
        result[i] = n % b + '0';
        n /= b;
    }

    result[i] = '\0';
    return result;
}

int main() {
    int b, c, i, n, s, t;
    FILE *fin = fopen("dualpal.in", "r");
    FILE *fout = fopen("dualpal.out", "w");
    fscanf(fin, "%d %d", &n, &s);

    for (t = 0, i = ++s; t < n; i++) {
        for (b = 2, c = 0; b <= 10; b++) {
            char *n = to_base(i, b);
            
            if (palindromic(n)) {
                if (++c >= 2) { 
                    t++; 
                    fprintf(fout, "%d\n", i);
                    break; 
                }
            }
            
            free(n);
        }
    }
    
    fclose(fin);
    fclose(fout);
    return 0;
}
