/*
ID: gsgorle1
LANG: C
TASK: namenum
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {
    int i, j, k, l;
    unsigned long long n;
    FILE *fin = fopen("namenum.in", "r");
    FILE *din = fopen("namenumdict.txt", "r"); // CHANGE BEFORE TURNIN to dict.txt
    FILE *fout = fopen("namenum.out", "w");

    char touchTone[10][3] = {
        {}, {},
        {'A', 'B', 'C'}, // 2  
        {'D', 'E', 'F'}, // 3
        {'G', 'H', 'I'}, // 4
        {'J', 'K', 'L'}, // 5
        {'M', 'N', 'O'}, // 6
        {'P', 'R', 'S'}, // 7
        {'T', 'U', 'V'}, // 8
        {'W', 'X', 'Y'}  // 9
    };
    int namesLen;
    int resultLen = 0;
    char result[5000][13];
    char names[5000][13];

    fscanf(fin, "%llu", &n);

    for (namesLen = 0; 0 < fscanf(din, "%s", &names[namesLen]); namesLen++);

    unsigned long long t = n;
    int nSize = 0;
    int serialNum[13];

    while (t > 0) {
        serialNum[nSize++] = t % 10;
        t /= 10;
    }
    
    for (i = nSize - 1, j = 0, k = nSize / 2; i >= k; i--, j++) {
        int tmp = serialNum[i];
        serialNum[i] = serialNum[j];
        serialNum[j] = tmp;
    }

    for (i = 0; i < namesLen; i++) {
        for (j = 0; j < 3; j++) {
            if (touchTone[serialNum[0]][j] == names[i][0] && 
                strlen(names[i]) == nSize) {

                int validName = 1;

                for (k = 1; k < nSize; k++) {
                    int validLetter = 0;

                    for (l = 0; l < 3; l++) {
                        if (touchTone[serialNum[k]][l] == names[i][k]) {
                            validLetter = 1;
                            break;
                        }
                    }

                    if (!validLetter) {
                        validName = 0;
                        break;
                    }
                }

                if (validName) {
                    strcpy(result[resultLen++], names[i]);
                }
            }
        }
    }

    if (resultLen) {
        for (i = 0; i < resultLen; i++) {
            fprintf(fout, "%s\n", result[i]);
        }
    }
    else {
        fprintf(fout, "NONE\n");
    }

    fclose(fin);
    fclose(din);
    fclose(fout);
    return 0;
}
