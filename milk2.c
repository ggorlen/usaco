/*
ID: gsgorle1
LANG: C
TASK: milk2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int start;
    int end;
} Range;

int cmp(const void *a, const void *b) {
   return ((Range*)a)->start - ((Range*)b)->start;
}

int main() {
    int i, n;
    int rLen = 0;
    Range r[5000];

    FILE *fin = fopen("milk2.in", "r");
    FILE *fout = fopen("milk2.out", "w");
    fscanf(fin, "%d", &n);

    for (i = 0; i < n; i++) {
        Range f;
        fscanf(fin, "%d %d", &f.start, &f.end);
        r[rLen++] = f;
    }
    
    qsort(r, rLen, sizeof(Range), cmp);
    int longestIdle = 0;
    int currStart = r[0].start;
    int currEnd = r[0].end;
    int longestMilked = currEnd - currStart;

    for (i = 1; i < rLen; i++) {
        if (r[i].start <= currEnd) {
            if (r[i].end > currEnd) {
                currEnd = r[i].end;
            }
        }
        else {
            if (r[i].start - currEnd > longestIdle) {
                longestIdle = r[i].start - r[i-1].end;
            }

            if (currEnd - currStart > longestMilked) {
                longestMilked = currEnd - currStart;
            }

            currStart = r[i].start;
            currEnd = r[i].end;
        }
    }

    if (currEnd - currStart > longestMilked) {
        longestMilked = currEnd - currStart;
    }
    
    fprintf(fout, "%d %d\n", longestMilked, longestIdle);
    fclose(fin);
    fclose(fout);
    exit(0);
    return 0;
}
