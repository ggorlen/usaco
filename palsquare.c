/*
ID: gsgorle1
LANG: C
TASK: palsquare
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int palindromic(char *s) {
    int i;
    int len = strlen(s);
    char buf[len];

    for (i = 0; i < len; i++) {
        buf[i] = s[i];
    }
    
    int half_len = len / 2;

    for (i = 0; i < half_len; i++) {
        if (buf[i] != buf[len-i-1]) {
            return 0;
        }
    }

    return 1;
}

char *to_base(int n, int b) {
    int i;
    char *result = malloc(sizeof(*result) * 80);
    char conversion[] = "0123456789ABCDEFGHIJK";

    for (i = 0; n > 0; i++) {
        result[i] = conversion[n%b];
        n /= b;
    }

    result[i] = '\0';
    return result;
}

void reverse(char *s) {
    int i;
    int len = strlen(s);
    int half = len / 2;

    for (i = 0; i < half; i++) {
        char tmp = s[i];
        s[i] = s[len-i-1];
        s[len-i-1] = tmp;
    }
}

int main() {
    int b, i;
    FILE *fin = fopen("palsquare.in", "r");
    FILE *fout = fopen("palsquare.out", "w");
    fscanf(fin, "%d", &b);

    for (i = 1; i <= 300; i++) {
        char *n = to_base(i * i, b);

        if (palindromic(n)) {
            char *m = to_base(i, b);
            reverse(m);
            fprintf(fout, "%s %s\n", m, n);
            free(m);
        }
    
        free(n);
    }
    
    fclose(fin);
    fclose(fout);
    return 0;
}
